package com.pack;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Demo_workshop {
	WebDriver driver=new ChromeDriver();
  @BeforeClass
  public void launchBrowser() {
	  
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
  }
  @Test(priority=1)
  public void login() {
	    driver.findElement(By.id("Email")).sendKeys("lokeshbabu@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Lokessh");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		WebElement text = driver.findElement(By.linkText("lokeshbabu@gmail.com"));
	  	System.out.println(text);
  }
  @Test(priority=2)
  public void selectDesktop() {
	  WebElement lap =driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		
      Actions a= new Actions(driver);
      a.moveToElement(lap).build().perform();
      
      driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Desktops')]")).click();
      
  }
  @Test(priority=3)
  public void sortProduct() {
	  WebElement e= driver.findElement(By.id("products-orderby"));
      Select s= new Select(e);
      s.selectByVisibleText("Price: Low to High");
      
  }
  @Test(priority=4)
  public void addToCart() {
	    driver.findElement(By.xpath("//h2[@class='product-title']//a[@href='/build-your-cheap-own-computer']")).click();
	       driver.findElement(By.id("add-to-cart-button-72")).click();

  }
  @Test(priority=5)
  public void purchase() {
	   driver.findElement(By.xpath("//span[contains(text(),'Shopping cart')]")).click();
	   driver.findElement(By.id("termsofservice")).click();
       driver.findElement(By.id("checkout")).click();
       WebElement e2 = driver.findElement(By.id("BillingNewAddress_CountryId"));
		Select sel=new Select(e2);
	  	driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
	  	driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();	
	  	driver.findElement(By.xpath("(//input[@value='Continue'])[3]")).click();
	  	driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
	  	driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
	  	driver.findElement(By.xpath("//input[@value='Confirm']")).click();
	  	
  }
  @Test(priority=6)
  public void orderStatus() {
	  WebElement txt = driver.findElement(By.xpath("//*[contains(text(),'Your order has been successfully processed!')]"));
	  	System.out.println(txt.getText());
  }
  
  @Test(priority=7)
  public void logout(){
	  driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
  }
  @AfterClass
  public void logOutAndCloseBrowser() {
	  	
	  	driver.close();

  }
}

