package com.pack;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Assignment {
	WebDriver driver= new ChromeDriver();
	@BeforeClass
	
	public void launch() {
		
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
	}
	
  @Test(priority=1)
  public void identify() {
	  WebElement id= driver.findElement(By.id("headerContainer"));
	  
	  String Expected_Title="Please identify yourself";
			String Actual_Title =id.getText();
			System.out.println(Actual_Title);
			
		   if(Actual_Title.equals(Expected_Title)) {
			   System.out.println("Pass");
		   }
		   else {
			   System.out.println("Fail");
		   }
		   
  }
		   @Test(priority=2)
		   public void icon() {
		 	  WebElement Icon= driver.findElement(By.className("atLogoImg"));
		 			boolean Actual_Title1 =Icon.isDisplayed();
		 			System.out.println(Actual_Title1);
		 			
		 			
		 			
		   driver.close();
	  
	  
	  
  }
}
